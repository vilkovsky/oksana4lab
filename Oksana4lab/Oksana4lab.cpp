﻿#include "pch.h"
#include <iostream>
const int LENGTH = 1024;
const char ADDSTR[4] = "не ";

using namespace std;

int spaceCounter = 0;

void addStrToText(char *newText, int space) {
	int lenAddstr = strlen(ADDSTR);
	int offset = spaceCounter * lenAddstr;

	for (int i = LENGTH - lenAddstr; i >= space; i--) {
		newText[i + lenAddstr + offset] = newText[i + offset];
	}

	for (int i = 0; i < lenAddstr; i++) {
		newText[space + i + offset] = ADDSTR[i];
	}

	spaceCounter++;
}

void findEnding(char *newText, const char *ending) {
	char text[LENGTH];
	strcpy_s(text, newText);
	spaceCounter = 0;
	int lenEnding = strlen(ending);
	int space = 0;

	for (int i = 0; i < LENGTH - lenEnding; i++) {
		if (text[i] == ' ')
			space = i + 1;

		for (int j = 0; j < lenEnding; j++) {
			if (text[i + j] != ending[j]) {
				break;
			}
			else {
				if (j == lenEnding - 1)
					addStrToText(newText, space);
			}
		}
	}
}

int main()
{
	char newText[LENGTH];

	system("chcp 1251");

	cout << "Введите текст : ";
	cin.getline(newText, sizeof(newText));

	findEnding(newText, "ить");
	findEnding(newText, "ать");
	findEnding(newText, "ять");
	findEnding(newText, "еть");

	cout << newText << endl;

	system("pause");
}

